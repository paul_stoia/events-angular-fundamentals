import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/RX';
import {IEvent, ISession} from './event.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';

@Injectable()
export class EventService {

  constructor(private http: HttpClient) {

  }

  getEvents(): Observable<IEvent[]> {
    // const subject = new Subject<IEvent[]>();
    // setTimeout(() => {
    //   subject.next(this.EVENTS);
    //   subject.complete();
    // }, 100);
    // return subject;
    return this.http.get<IEvent[]>('/api/events')
      .pipe(tap(events => {
        localStorage.setItem('allEvents', JSON.stringify(events));
      }))
      .pipe(catchError(this.handleError<IEvent[]>('getEvents', [])));
  }

  getEvent(id: number): Observable<IEvent> {
    // return this.EVENTS.find(event => event.id === id);
    return this.http.get<IEvent>('/api/events/' + id)
      .pipe(catchError(this.handleError<IEvent>('getEvent')));
  }

  saveEvent(event) {
    const existingEvents: IEvent[] = JSON.parse(localStorage.getItem('allEvents'));
    if (existingEvents) {
      const index: number = existingEvents.findIndex(existingEvent => existingEvent.id === event.id);
      if (index >= 0) {
        existingEvents.splice(index, 1, event);
      } else {
        existingEvents.push(event);
      }
      localStorage.setItem('allEvents', JSON.stringify(existingEvents));
    } else {
      localStorage.setItem('allEvents', JSON.stringify([event]));
    }
    const options = {headers: new HttpHeaders({'Content-Type': 'application/json'})};
    return this.http.post<IEvent>('/api/events', event, options)
      .pipe(catchError(this.handleError<IEvent>('saveEvent')));
  }

  // This is the old way: now the server knows if an event has an id that it will an update, otherwise a create operation. Usually for
  // update we use PUT, but now we can call saveEvent using POST for both
  // updateEvent(event: IEvent) {
  //   const index = this.EVENTS.findIndex(x => x.id === event.id);
  //   this.EVENTS[index] = event;
  // }

  searchSessions(searchTerm: string): Observable<ISession[]> {
    // const term = searchTerm.toLocaleLowerCase();
    // let results: ISession[] = [];
    //
    // this.EVENTS.forEach(event => {
    //   let matchingSessions = event.sessions.filter(session => session.name.toLocaleLowerCase().indexOf(term) > -1);
    //   matchingSessions = matchingSessions.map((session: any) => {
    //     session.eventId = event.id;
    //     return session;
    //   });
    //   results = results.concat(matchingSessions);
    // });
    //
    // const emitter = new EventEmitter(true);
    // setTimeout(() => {
    //   emitter.emit(results);
    // }, 100);
    // return emitter;
    return this.http.get<ISession[]>('/api/sessions/search?search=' + searchTerm)
      .pipe(catchError(this.handleError<ISession[]>('searchSessions', [])));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log('Caught error in' + operation + ' operation: ' + error);
      return Observable.of(result as T);
    };
  }
}
