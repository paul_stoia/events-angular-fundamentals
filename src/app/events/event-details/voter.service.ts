import {Injectable} from '@angular/core';
import {ISession} from '../shared';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from '../../../../node_modules/rxjs';
import {catchError} from 'rxjs/operators';

@Injectable()
export class VoterService {
  constructor(private http: HttpClient) {

  }

  addVoter(eventId: number, session: ISession, voterName: string): void {
    session.voters.push(voterName);

    const options = {headers: new HttpHeaders({'Content-Type': '/application/json'})};
    const url = `/api/events/${eventId}/sessions/${session.id}/voters/${voterName}`;
    this.http.post(url, {}, options)
      .pipe(catchError(this.handleError('addVoter')))
      .subscribe();
  }

  deleteVoter(eventId: number, session: ISession, voterName: string): void {
    session.voters = session.voters.filter(voter => voter !== voterName);
    const url = `/api/events/${eventId}/sessions/${session.id}/voters/${voterName}`;
    this.http.delete(url)
      .pipe(catchError(this.handleError('deleteVoter')))
      .subscribe();
  }

  userHasVoted(session: ISession, voterName: string): boolean {
    return session.voters.some(voter => voter === voterName);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log('Caught error in' + operation + ' operation: ' + error);
      return Observable.of(result as T);
    };
  }
}
