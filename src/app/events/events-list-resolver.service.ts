import {Resolve} from '@angular/router';
import {Injectable} from '@angular/core';
import {EventService} from './shared/event.service';

@Injectable()
export class EventsListResolverService implements Resolve<any> {

  constructor(private eventService: EventService) {

  }

  // avoid partially loading of the page
  // Because this service is a resolver, it automatically subscribe to the Observable returned by getEvents, so we don't have to subscribe
  // it manually (however, if there is not a subscription then the observable call is not make).
  resolve() {
    return this.eventService.getEvents();
  }

}
