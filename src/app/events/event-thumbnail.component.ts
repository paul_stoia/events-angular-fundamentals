import {Component, Input} from '@angular/core';
import {IEvent} from './shared';

@Component({
  selector: 'event-thumbnail',
  template: `
    <div [routerLink]="['/events', event.id]" class="well hoverwell thumbnail">
      <h2>{{event?.name | uppercase}}</h2>
      <div>Date: {{event?.date | date:'shortDate'}}</div>
      <!--<div [ngSwitch]="event?.time" [ngClass]="getStartTimeClass()">-->
      <div [ngSwitch]="event?.time" [ngStyle]="getStartTimeStyle()">
        Time: {{event?.time}}
        <span *ngSwitchCase="'8:00 am'">(Early start)</span>
        <span *ngSwitchCase="'10:00 am'">(Late start)</span>
        <span *ngSwitchDefault>(Normal start)</span>
      </div>
      <div>Price: {{event?.price | currency:'USD'}}</div>
      <div *ngIf="event?.location">
        <!--<div [hidden]="!event?.location">-->
        <span>Location: {{event?.location?.address}}</span>
        <span class="pad-left">{{event?.location?.city}}, {{event?.location?.country}}</span>
      </div>
      <div *ngIf="event?.onlineUrl">
        <!--<div [hidden]="!event?.onlineUrl">-->
        Online URL: {{event?.onlineUrl}}
      </div>
    </div>
  `,
  styles: [`
    .green {
      color: #003300 !important;
    }

    .bold {
      font-weight: bold;
    }

    .thumbnail {
      min-height: 210px;
    }

    .pad-left {
      margin-left: 10px;
    }

    .well div {
      color: #BBB;
    }
  `]
})
export class EventThumbnailComponent {

  @Input() event: IEvent;

  getStartTimeClass() {
    // const isEarlyStart = this.event && this.event.time === '8:00 am';
    // return {green: isEarlyStart, bold: isEarlyStart};

    if (this.event && this.event.time === '8:00 am') {
      // return 'green bold';
      return ['green', 'bold'];
    }
    // return '';
    return [];
  }

  getStartTimeStyle(): any {
    if (this.event && this.event.time === '8:00 am') {
      return {color: '#003300', 'font-weight': 'bold'};
    }
    return {};
  }
}
