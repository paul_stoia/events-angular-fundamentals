import {Routes} from '@angular/router';
import {
  CreateEventComponent,
  CreateSessionComponent,
  EventDetailsComponent,
  EventsListComponent,
  EventsListResolverService,
  EventsResolverService
} from './events/index';
import {Error404Component} from './errors/404.component';
import {NavBarComponent} from './nav/navbar.component';

export const appRoutes: Routes = [
  // THE ORDER MATTERS HERE FOR MATCHING PATH !!!
  {path: 'events/new', component: CreateEventComponent, canDeactivate: ['canDeactivateCreateEvent']},
  // resolve allows you to prefetch data for a component (for this example: wait until all data is loaded and just then display in the page
  // the data!
  // EventsListResolverService represents the resolver which is doing it's job before EventsListComponent. EventsListResolverService get the
  // data and puts it in the route with the property name given below: events.
  // We get the data only once from resolver and pass it into the component through route
  {path: 'events', component: EventsListComponent, resolve: {events: EventsListResolverService}},
  // this is the old way
  // {path: 'events/:id', component: EventDetailsComponent, canActivate: [EventRouteActivatorService]},
  {path: 'events/:id', component: EventDetailsComponent, resolve: {event: EventsResolverService}},
  {path: 'events/session/new', component: CreateSessionComponent},
  {path: '404', component: Error404Component},
  {path: '', redirectTo: '/events', pathMatch: 'full'},
  // Angular will parse this string with the delimiter #. The first part is the math to the module and the second one is the name of the
  // module. When a route starts with 'user' load this module from this path!
  {path: 'user', loadChildren: 'app/user/user.module#UserModule'}
];
