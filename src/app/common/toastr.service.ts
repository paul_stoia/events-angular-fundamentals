import {InjectionToken} from '@angular/core';

// InjectionToken creates a token used for the dependency injection registry in order to find the instance for the service that we want
export let TOASTR_TOKEN = new InjectionToken<Toastr>('toastr');

export interface Toastr {
  success(msg: string, title?: string): void;
  info(msg: string, title?: string): void;
  warning(msg: string, title?: string): void;
  error(msg: string, title?: string): void;
}

