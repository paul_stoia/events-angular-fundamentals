import {Component, OnInit} from '@angular/core';
import {AuthService} from '../user/auth.service';
import {EventService, IEvent} from '../events/shared';

@Component({
  selector: 'nav-bar',
  templateUrl: './navbar.component.html',
  styles: [`
    .nav.navbar-nav {
      font-size: 15px;
    }

    #searchForm {
      margin-right: 100px;
    }

    @media (max-width: 1200px) {
      #searchForm {
        display: none
      }
    }

    li > a.active {
      color: #F97924;
    }
  `]
})
export class NavBarComponent implements OnInit {

  searchTerm = '';
  foundSessions: any[];
  events: IEvent[];

  constructor(protected authService: AuthService, private eventService: EventService) {

  }

  ngOnInit(): void {
    this.refreshEvents();
  }

  refreshEvents() {
    if (localStorage.getItem('allEvents')) {
      this.events = JSON.parse(localStorage.getItem('allEvents'));
      console.log('Take events from STORAGE');
    } else {
      this.eventService.getEvents().subscribe(data => {
        this.events = data;
        console.log('Take events from SERVICE');
      });
    }
  }

  searchSessions(searchTerm) {
    this.eventService.searchSessions(searchTerm).subscribe(sessions => this.foundSessions = sessions);
  }
}
