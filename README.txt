To install server command: npm install ngf-server -S
To start server command: npm run server
To start app: npm start
To run tests: npm test

To show TSLint (Type Script Linting): ng lint
To fix all TSLint issues in entire project: ng lint --fix
To go to production: npm run build (ng build --prod)

Login username: johnpapa (the password does not matter)